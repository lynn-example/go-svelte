package middleware

// go get github.com/gin-contrib/sessions
import (
	"os"
	"github.com/joho/godotenv"
	"github.com/gin-gonic/gin"
    "github.com/gin-contrib/sessions"
    "github.com/gin-contrib/sessions/cookie"
)

/*
***** 參考來源 *****
** 原文作者：aen233
** 轉自連結：https://learnku.com/articles/33876
*******************
*/

// 使用 Cookie 保存 Session
func EnableCookieSession() gin.HandlerFunc {
    env_err := godotenv.Load()
	if env_err != nil {
		panic(env_err.Error())
	}

	appKey := os.Getenv("APP_KEY")
	sessName := "sess_test"

	store := cookie.NewStore([]byte(appKey))

	return sessions.Sessions(sessName, store)	
}
