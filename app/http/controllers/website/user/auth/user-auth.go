package user_auth

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"net/url"
	// custom
	"shop/app/core-module/auth"
	"shop/app/shop-module/user/service"
)

type UserData struct {
	Id 	  	string `json:"Id"`
	Email  	string `json:"Email"`
}

// 會員登入
type signInForm struct {
	Account  	string `json:"account" form:"account"`
	Password    string `json:"password" form:"password"`
}

// 會員註冊
type signUpForm struct {
	Email  				    string `json:"email" form:"email"`
	Password    			string `json:"password" form:"password"`
	Password_Confirmation	string `json:"password_confirmation" form:"password_confirmation"`
}

type UserAuthResponse struct {
	Status 	string 	 `json:"Status"`
	Message string   `json:"Message"`
	Data 	UserData `json:"Data"`
}

func UserAuthSignUpPage(c *gin.Context) {
	c.HTML(http.StatusOK, "sign-up.tmpl", gin.H{
		"title": "會員註冊畫面",
	})
}

func UserAuthSignInPage(c *gin.Context) {
	c.HTML(http.StatusOK, "sign-in.tmpl", gin.H{
		"title": "會員登入畫面",
	})
}

func UserAuthSignInProcess(c *gin.Context) {
	var allowInputForm signInForm

	if c.BindJSON(&allowInputForm) != nil {
	    c.JSON(http.StatusBadRequest, gin.H{"message": "Invalid request form"})
	    c.Abort()
	    return
	}

	allowInput := map[string]interface{}{
		"email": allowInputForm.Account,
		"password": allowInputForm.Password,
	}

	// 登入使用者
	UserAuthServiceResult := user_service.WebsiteAttemptLogin(allowInput)

	// 回應資料
	responseData := UserAuthResponse{}
	if UserAuthServiceResult.Status == "success" {
		// todo: 紀錄登入成功

		auth.SaveAuthSession(c, allowInputForm.Account)

		responseData.Status 	= "success"
		responseData.Data.Id 	= UserAuthServiceResult.Data["serial_number"].(string)
		responseData.Data.Email = UserAuthServiceResult.Data["email"].(string)
	} else {
		responseData.Status  = "fail"
		responseData.Message = UserAuthServiceResult.Message
	}

	c.JSON(http.StatusOK, responseData)
}

func UserAuthSignUpProcess(c *gin.Context) {
	var allowInputForm signUpForm

	if c.BindJSON(&allowInputForm) != nil {
	    c.JSON(http.StatusBadRequest, gin.H{"message": "Invalid request form"})
	    c.Abort()
	    return
	}

	responseData := UserAuthResponse{}

	allowInput := map[string]interface{}{
		"email"					: allowInputForm.Email,
		"password"				: allowInputForm.Password,
		"password_confirmation"	: allowInputForm.Password_Confirmation,
	}

	// 註冊
	UserAuthServiceResult := user_service.WebsiteCreateNormalUser(allowInput)

	if UserAuthServiceResult.Status == "success" {
		auth.SaveAuthSession(c, allowInputForm.Email)

		responseData.Status 	= "success"
		responseData.Data.Id 	= UserAuthServiceResult.Data["serial_number"].(string)
		responseData.Data.Email = UserAuthServiceResult.Data["email"].(string)
	} else  {
		responseData.Status  = "fail"
		responseData.Message = UserAuthServiceResult.Message
	}

	c.JSON(http.StatusOK, responseData)
}

func UserAuthSignOutProcess(c *gin.Context) {
	if hasSession := auth.HasSession(c); hasSession == true {
		auth.ClearAuthSession(c)
	}

	location := url.URL{Path: "/user/auth/sign-in",}
    c.Redirect(http.StatusFound, location.RequestURI())
}



