package core_string

import(
	"math/rand"
	"time"
)

const STRING_CANDIDATE_TEXT = "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ"

var STRING_SEEDEDRAND *rand.Rand = rand.New(rand.NewSource(time.Now().UnixNano()))

func RandomString(rand_str_length int) (string) {
	// 候選文字長度
	rand_str := make([]byte, rand_str_length)
	candidate_text := STRING_CANDIDATE_TEXT

	// 隨機挑選文字
	for i := range rand_str {
		rand_str[i] = candidate_text[STRING_SEEDEDRAND.Intn(len(candidate_text))]
	}

    return string(rand_str)
}