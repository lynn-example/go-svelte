package core_crypto

import(
	// "fmt"
    "crypto/sha1"
    "encoding/hex"
)

// 參考來源：https://stackoverflow.com/questions/10701874/generating-the-sha-hash-of-a-string-using-golang
func SHA1(str string) (string) {
	h := sha1.New()
    h.Write([]byte(str))
    sha1_hash := hex.EncodeToString(h.Sum(nil))

    return sha1_hash
}

