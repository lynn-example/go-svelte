package auth

// go get github.com/gin-contrib/sessions
import (
	"github.com/gin-gonic/gin"
    "github.com/gin-contrib/sessions"
    "net/http"
    "net/url"
)

/*
***** 參考來源 *****
** 原文作者：aen233
** 轉自連結：https://learnku.com/articles/33876
*******************
*/

// session 中間件
func AuthSessionMiddle() gin.HandlerFunc {
	return func(c *gin.Context) {
		session := sessions.Default(c)
		sessionValue := session.Get("user_email")

		if sessionValue == nil {
			c.JSON(http.StatusUnauthorized, gin.H {
				"error": "Unauthorized",
			})

			c.Abort()
			return 
		}

		c.Set("user_email", sessionValue.(string))

		c.Next()
		return
	}
}

// 註冊與登錄都要保存 Session
func SaveAuthSession(c *gin.Context, email string) {
	session := sessions.Default(c)
	session.Set("user_email", email)
	session.Save()
}

func CheckAuthSession() gin.HandlerFunc {
	return func(c *gin.Context) {
		hasSession := HasSession(c)

		if hasSession {
			location := url.URL{Path: "/index",}
	    	c.Redirect(http.StatusFound, location.RequestURI())
			c.Abort()

			return 
		} else {
			c.Next()
			return
		}
	}


}

func ClearAuthSession(c *gin.Context) {
	session := sessions.Default(c)
	session.Clear()
	session.Save()
}

func HasSession(c *gin.Context) bool {
	session := sessions.Default(c)

	sessionValue := session.Get("user_email")

	if sessionValue == nil {
		return false
	}

	return true
}

func GetSessionUserEmail(c *gin.Context) string {
	session := sessions.Default(c)
	sessionValue := session.Get("user_email")

	if sessionValue == nil {
		return "0"
	}

	return sessionValue.(string)
}

func GetUserSession(c *gin.Context) map[string]interface{} {
	hasSession := HasSession(c)
	userEmail := ""

	if hasSession {
		userEmail = GetSessionUserEmail(c)
	}

	data := make(map[string]interface{})
	data["hasSession"] = hasSession
	data["userEmail"]  = userEmail

	return data
}


