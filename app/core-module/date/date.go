package core_date

import "time"

func GetNow() (string) {
	currentTime := time.Now()
    return currentTime.Format("2006-01-02 15:04:05") // Golang 定義的時間格式
}

