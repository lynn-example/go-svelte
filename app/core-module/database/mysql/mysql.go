package db_mysql

import (
	"os"
	"fmt"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"github.com/joho/godotenv"
)

func DBConnect() (*sql.DB) {
    env_err := godotenv.Load()
	if env_err != nil {
		panic(env_err.Error())
	}

	conn := fmt.Sprintf("%s:%s@%s(%s:%s)/%s", 
		os.Getenv("DB_USERNAME"), 
		os.Getenv("DB_PASSWORD"), 
		os.Getenv("DB_NETWORK"), 
		os.Getenv("DB_HOST"), 
		os.Getenv("DB_PORT"), 
		os.Getenv("DB_DATABASE"), 
	)

	dbConnect, db_err := sql.Open(
		os.Getenv("DB_CONNECTION"),
		conn,
	)

	if db_err != nil {
		panic(db_err.Error())
	}

	db_err = dbConnect.Ping()
	if db_err != nil {
		panic(db_err.Error())
	}

	dbConnect.SetMaxOpenConns(10) // 可設置最大DB連線數，設<=0則無上限（連線分成 in-Use正在執行任務 及 idle執行完成後的閒置 兩種）  
	dbConnect.SetMaxIdleConns(10) // 設置最大idle閒置連線數。
	// dbConnect.Exec("USE `table_name`") // 設置使用的資料庫名稱

	return dbConnect
}


