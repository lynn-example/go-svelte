package db_mysql

import (
	"os"
	"fmt"
	// "time"
	"database/sql"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"github.com/joho/godotenv"
)

var globalDB *gorm.DB

func GORMConnect() (*gorm.DB) {
    env_err := godotenv.Load()
	if env_err != nil {
		panic(env_err.Error())
	}

	conn := fmt.Sprintf("%s:%s@%s(%s:%s)/%s?charset=utf8&parseTime=True", 
		os.Getenv("DB_USERNAME"), 
		os.Getenv("DB_PASSWORD"), 
		os.Getenv("DB_NETWORK"), 
		os.Getenv("DB_HOST"), 
		os.Getenv("DB_PORT"), 
		os.Getenv("DB_DATABASE"), 
	)

	sqlDB, db_err := sql.Open("mysql", conn)

	if db_err != nil {
		fmt.Println("connection to mysql failed:", db_err)
		panic(db_err.Error())
	}

	gormDB, gorm_db_err := gorm.Open(mysql.New(mysql.Config{
	  Conn: sqlDB,
	}), &gorm.Config{})

	if gorm_db_err != nil {
		fmt.Println("connection to mysql failed:", gorm_db_err)
		panic(db_err.Error())
	}

	globalDB = gormDB

	// dbConnect.SetConnMaxLifetime(time.Hour)
	// dbConnect.SetMaxIdleConns(10)
	// dbConnect.SetMaxOpenConns(100)

	return gormDB
}

func GetGormDB() (*gorm.DB) {
	return globalDB
}


