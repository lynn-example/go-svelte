package core_data_support

import(
	// "fmt"
	"math/rand"
	"time"
)

// https://blog.kalan.dev/golang-default-value/
type RandStr struct {
	Prefix 			string 	`default:""`
	Length 			int64 	`default:10`
	ConnectSymble 	string  `default:""`
	Text 			string 	`default:"abcdefghijklmnopqrstuvwxyz1234567890"`
}

const UUID_CONNECT_SYMBLE = ""
const UUID_CANDIDATE_TEXT = "abcdefghijklmnopqrstuvwxyz1234567890"

var SEEDEDRAND *rand.Rand = rand.New(rand.NewSource(time.Now().UnixNano()))

func GenerateRandomStringAppendPrefix(rand_str_data RandStr) (string) {
	if rand_str_data.Text == "" {
		rand_str_data.Text = UUID_CANDIDATE_TEXT
	}

	rand_str := GenerateRandomString(rand_str_data.Length, rand_str_data.Text)

	return rand_str_data.Prefix + rand_str_data.ConnectSymble + rand_str
}

func GenerateRandomString(rand_str_length int64, candidate_text string) (string) {
	// 候選文字長度
	rand_str := make([]byte, rand_str_length)

	// 隨機挑選文字
	for i := range rand_str {
		rand_str[i] = candidate_text[SEEDEDRAND.Intn(len(candidate_text))]
	}

    return string(rand_str)
}

