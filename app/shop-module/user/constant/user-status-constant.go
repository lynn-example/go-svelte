package user_constant

const (
	USER_STATUS_UNDONE	= "U"
	USER_STATUS_ENABLE	= "E"
	USER_STATUS_DISABLE	= "D"
)