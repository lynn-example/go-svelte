package user_service

import (
	"database/sql"
	"golang.org/x/crypto/bcrypt"
	"shop/app/core-module/date"
	"shop/app/core-module/string"
	"shop/app/shop-module/user/repository"
	"shop/app/shop-module/user/support"
)

type UserServiceResponse struct {
	Status 	string 	 `json:"Status"`
	Message string 	 `json:"Message"`
	Data 	map[string]interface{} `json:"Data"`
}

func WebsiteAttemptLogin(input map[string]interface{}) UserServiceResponse {
	data := UserServiceResponse{
		Status	: "success",
		Message	: "",
		Data 	: map[string]interface{}{},
	}

	// 透過 Email 找使用者
	User := user_repository.FindUserByEmail(input["email"].(string))

	// 檢查使用者密碼
	if len(User) != 0 {
		if !IsSame(input["password"].(string), User["password"].(string)) {
			data.Status = "fail"
			data.Message = "帳號或密碼錯誤"

			return data
		}
	}

	data.Data = User

	return data
}

func WebsiteCreateNormalUser(input map[string]interface{}) UserServiceResponse {
	data := UserServiceResponse{
		Status	: "success",
		Message	: "",
		Data 	: map[string]interface{}{},
	}

	// 檢查密碼是否相同
	if input["password"].(string) != input["password_confirmation"].(string) {
		data.Status = "fail"
		data.Message = "密碼或再確認密碼不一致"

		return data
	}

	// 檢查帳號是否存在
	User := user_repository.FindUserByEmail(input["email"].(string))
	if len(User) != 0 {
		data.Status = "fail"
		data.Message = "帳號已存在"

		return data
	}

	// 會員資料
	var user_data = user_support.GetUserDefaultValue()
	user_data["email"] 			= input["email"].(string)
	user_data["last_login_at"]  = core_date.GetNow()

	if line_id, ok := input["line_id"].(string); !ok {
		user_data["line_id"] = sql.NullString{user_data["line_id"].(string), false}
	} else {
		user_data["line_id"] = line_id
	}

	// 會員編號
	user_data["id"] = user_support.GenerateUserId()

	// 加密密碼
	if hash_password, hash_err := Hash(input["password"].(string)); hash_err == nil {
        user_data["password"] = hash_password
    }

    User = user_repository.CreateUser(user_data)

	if len(User) == 0 {
		data.Status = "fail"
		data.Message = "註冊失敗"
	} else {
		data.Data = User
	}

	return data
}

func FindOrFallUserByEmail(email string) (map[string]interface{}) {
	User := user_repository.FindUserByEmail(email)
	return User
}

func Hash(str string) (string, error) {
    hashed, err := bcrypt.GenerateFromPassword([]byte(str), bcrypt.DefaultCost)
    return string(hashed), err
}

func IsSame(str string, hashed string) bool {
    return bcrypt.CompareHashAndPassword([]byte(hashed), []byte(str)) == nil
}