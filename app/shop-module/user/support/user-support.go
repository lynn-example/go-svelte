package user_support

import (
	"shop/app/core-module/data/support"
	user_constant "shop/app/shop-module/user/constant"
)

func GetUserDefaultValue() map[string]interface{} {
	return map[string]interface{}{
		"email"					: user_constant.USER_EMAIL,
		"password"				: user_constant.USER_PASSWORD,
		"status"				: user_constant.USER_STATUS,
		"line_id"				: user_constant.USER_LINE_ID,
		"last_login_at"			: user_constant.USER_LAST_LOGIN_AT,
		"email_verify_status"	: user_constant.USER_EMAIL_VERIFY_STATUS,
	}
}

func GenerateUserId() (string) {
	var rand_str core_data_support.RandStr
	rand_str.Length = user_constant.USER_ID_LENGTH

	id := core_data_support.GenerateRandomStringAppendPrefix(rand_str)

	return id
}


