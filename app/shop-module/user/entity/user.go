package user_entity

import (
	"fmt"
	"shop/app/core-module/database/mysql"
)

func TableName() string {
  return "users"
}

func Create(data map[string]interface{}) map[string]interface{} {
	now := time.Now().Format("2006-01-02 15:04:05")

	data["created_at"] = now
	data["updated_at"] = now

	tableName := TableName()

	gormDB := db_mysql.GetGormDB()
	result := gormDB.Debug().Table(tableName).Create(&data)

	fmt.Println(data["id"].(string), result.Error, result.RowsAffected)

	User := map[string]interface{}{}
	if result.Error == nil {
		if result.RowsAffected > 0 {
			gormDB.Debug().Table(tableName).Where("id = ?", data["id"].(string)).Find(&User)
		}
	}

	return User
}


