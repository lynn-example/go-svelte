package user_repository

import (
	"time"
	"shop/app/core-module/database/mysql"
	"shop/app/shop-module/user/entity"
)

var tableName = user_entity.TableName()

func FindUserByEmail(email string) (map[string]interface{}) {
	result := map[string]interface{}{}

	gormDB := db_mysql.GetGormDB()

	gormDB.Debug().Table(tableName).Where("email = ?", email).Find(&result)

	return result
}

func CreateUser(data map[string]interface{}) map[string]interface{} {
	User := user_entity.Create(data)

	return User
}