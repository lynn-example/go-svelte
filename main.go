package main

import (
	"github.com/gin-gonic/gin"
	// custom
	"shop/app/core-module/database/mysql"
	"shop/routes/website"
	"shop/routes/website/user"
)

func main() {
	db_mysql.GORMConnect()

	router := gin.Default()
	router.Use(gin.Logger())
	router.LoadHTMLGlob("./resources/views/***/**/*") // 載兩層模板路徑..
	router.Static("/asset", "./public/assets/svelte/website") // 設定使用 js 及 css 路徑

	main_router.Router(router)
	user_router.Router(router)

	router.Run(":8586")
}