import App from './App.svelte';

const app = new App({
	target: document.getElementById('signUpContainer'),
	props: {
		name: 'Sign Up'
	}
});

export default app;
