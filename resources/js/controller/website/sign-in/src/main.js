import App from './App.svelte';

const app = new App({
	target: document.getElementById('signInContainer'),
	props: {
		name: 'Sign In'
	}
});

export default app;
