import userSignIn from './website/user-sign-in';
import userSignUp from './website/user-sign-up';

export default [
	userSignUp,
	userSignIn,
];