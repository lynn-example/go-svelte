
(function(l, r) { if (l.getElementById('livereloadscript')) return; r = l.createElement('script'); r.async = 1; r.src = '//' + (window.location.host || 'localhost').split(':')[0] + ':35730/livereload.js?snipver=1'; r.id = 'livereloadscript'; l.getElementsByTagName('head')[0].appendChild(r) })(window.document);
var app = (function () {
    'use strict';

    function noop() { }
    function add_location(element, file, line, column, char) {
        element.__svelte_meta = {
            loc: { file, line, column, char }
        };
    }
    function run(fn) {
        return fn();
    }
    function blank_object() {
        return Object.create(null);
    }
    function run_all(fns) {
        fns.forEach(run);
    }
    function is_function(thing) {
        return typeof thing === 'function';
    }
    function safe_not_equal(a, b) {
        return a != a ? b == b : a !== b || ((a && typeof a === 'object') || typeof a === 'function');
    }
    function is_empty(obj) {
        return Object.keys(obj).length === 0;
    }

    function append(target, node) {
        target.appendChild(node);
    }
    function insert(target, node, anchor) {
        target.insertBefore(node, anchor || null);
    }
    function detach(node) {
        node.parentNode.removeChild(node);
    }
    function element(name) {
        return document.createElement(name);
    }
    function text(data) {
        return document.createTextNode(data);
    }
    function space() {
        return text(' ');
    }
    function listen(node, event, handler, options) {
        node.addEventListener(event, handler, options);
        return () => node.removeEventListener(event, handler, options);
    }
    function attr(node, attribute, value) {
        if (value == null)
            node.removeAttribute(attribute);
        else if (node.getAttribute(attribute) !== value)
            node.setAttribute(attribute, value);
    }
    function children(element) {
        return Array.from(element.childNodes);
    }
    function set_input_value(input, value) {
        input.value = value == null ? '' : value;
    }
    function custom_event(type, detail) {
        const e = document.createEvent('CustomEvent');
        e.initCustomEvent(type, false, false, detail);
        return e;
    }

    let current_component;
    function set_current_component(component) {
        current_component = component;
    }

    const dirty_components = [];
    const binding_callbacks = [];
    const render_callbacks = [];
    const flush_callbacks = [];
    const resolved_promise = Promise.resolve();
    let update_scheduled = false;
    function schedule_update() {
        if (!update_scheduled) {
            update_scheduled = true;
            resolved_promise.then(flush);
        }
    }
    function add_render_callback(fn) {
        render_callbacks.push(fn);
    }
    let flushing = false;
    const seen_callbacks = new Set();
    function flush() {
        if (flushing)
            return;
        flushing = true;
        do {
            // first, call beforeUpdate functions
            // and update components
            for (let i = 0; i < dirty_components.length; i += 1) {
                const component = dirty_components[i];
                set_current_component(component);
                update(component.$$);
            }
            set_current_component(null);
            dirty_components.length = 0;
            while (binding_callbacks.length)
                binding_callbacks.pop()();
            // then, once components are updated, call
            // afterUpdate functions. This may cause
            // subsequent updates...
            for (let i = 0; i < render_callbacks.length; i += 1) {
                const callback = render_callbacks[i];
                if (!seen_callbacks.has(callback)) {
                    // ...so guard against infinite loops
                    seen_callbacks.add(callback);
                    callback();
                }
            }
            render_callbacks.length = 0;
        } while (dirty_components.length);
        while (flush_callbacks.length) {
            flush_callbacks.pop()();
        }
        update_scheduled = false;
        flushing = false;
        seen_callbacks.clear();
    }
    function update($$) {
        if ($$.fragment !== null) {
            $$.update();
            run_all($$.before_update);
            const dirty = $$.dirty;
            $$.dirty = [-1];
            $$.fragment && $$.fragment.p($$.ctx, dirty);
            $$.after_update.forEach(add_render_callback);
        }
    }
    const outroing = new Set();
    function transition_in(block, local) {
        if (block && block.i) {
            outroing.delete(block);
            block.i(local);
        }
    }

    const globals = (typeof window !== 'undefined'
        ? window
        : typeof globalThis !== 'undefined'
            ? globalThis
            : global);
    function mount_component(component, target, anchor) {
        const { fragment, on_mount, on_destroy, after_update } = component.$$;
        fragment && fragment.m(target, anchor);
        // onMount happens before the initial afterUpdate
        add_render_callback(() => {
            const new_on_destroy = on_mount.map(run).filter(is_function);
            if (on_destroy) {
                on_destroy.push(...new_on_destroy);
            }
            else {
                // Edge case - component was destroyed immediately,
                // most likely as a result of a binding initialising
                run_all(new_on_destroy);
            }
            component.$$.on_mount = [];
        });
        after_update.forEach(add_render_callback);
    }
    function destroy_component(component, detaching) {
        const $$ = component.$$;
        if ($$.fragment !== null) {
            run_all($$.on_destroy);
            $$.fragment && $$.fragment.d(detaching);
            // TODO null out other refs, including component.$$ (but need to
            // preserve final state?)
            $$.on_destroy = $$.fragment = null;
            $$.ctx = [];
        }
    }
    function make_dirty(component, i) {
        if (component.$$.dirty[0] === -1) {
            dirty_components.push(component);
            schedule_update();
            component.$$.dirty.fill(0);
        }
        component.$$.dirty[(i / 31) | 0] |= (1 << (i % 31));
    }
    function init(component, options, instance, create_fragment, not_equal, props, dirty = [-1]) {
        const parent_component = current_component;
        set_current_component(component);
        const prop_values = options.props || {};
        const $$ = component.$$ = {
            fragment: null,
            ctx: null,
            // state
            props,
            update: noop,
            not_equal,
            bound: blank_object(),
            // lifecycle
            on_mount: [],
            on_destroy: [],
            before_update: [],
            after_update: [],
            context: new Map(parent_component ? parent_component.$$.context : []),
            // everything else
            callbacks: blank_object(),
            dirty,
            skip_bound: false
        };
        let ready = false;
        $$.ctx = instance
            ? instance(component, prop_values, (i, ret, ...rest) => {
                const value = rest.length ? rest[0] : ret;
                if ($$.ctx && not_equal($$.ctx[i], $$.ctx[i] = value)) {
                    if (!$$.skip_bound && $$.bound[i])
                        $$.bound[i](value);
                    if (ready)
                        make_dirty(component, i);
                }
                return ret;
            })
            : [];
        $$.update();
        ready = true;
        run_all($$.before_update);
        // `false` as a special case of no DOM component
        $$.fragment = create_fragment ? create_fragment($$.ctx) : false;
        if (options.target) {
            if (options.hydrate) {
                const nodes = children(options.target);
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment && $$.fragment.l(nodes);
                nodes.forEach(detach);
            }
            else {
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment && $$.fragment.c();
            }
            if (options.intro)
                transition_in(component.$$.fragment);
            mount_component(component, options.target, options.anchor);
            flush();
        }
        set_current_component(parent_component);
    }
    /**
     * Base class for Svelte components. Used when dev=false.
     */
    class SvelteComponent {
        $destroy() {
            destroy_component(this, 1);
            this.$destroy = noop;
        }
        $on(type, callback) {
            const callbacks = (this.$$.callbacks[type] || (this.$$.callbacks[type] = []));
            callbacks.push(callback);
            return () => {
                const index = callbacks.indexOf(callback);
                if (index !== -1)
                    callbacks.splice(index, 1);
            };
        }
        $set($$props) {
            if (this.$$set && !is_empty($$props)) {
                this.$$.skip_bound = true;
                this.$$set($$props);
                this.$$.skip_bound = false;
            }
        }
    }

    function dispatch_dev(type, detail) {
        document.dispatchEvent(custom_event(type, Object.assign({ version: '3.31.0' }, detail)));
    }
    function append_dev(target, node) {
        dispatch_dev('SvelteDOMInsert', { target, node });
        append(target, node);
    }
    function insert_dev(target, node, anchor) {
        dispatch_dev('SvelteDOMInsert', { target, node, anchor });
        insert(target, node, anchor);
    }
    function detach_dev(node) {
        dispatch_dev('SvelteDOMRemove', { node });
        detach(node);
    }
    function listen_dev(node, event, handler, options, has_prevent_default, has_stop_propagation) {
        const modifiers = options === true ? ['capture'] : options ? Array.from(Object.keys(options)) : [];
        if (has_prevent_default)
            modifiers.push('preventDefault');
        if (has_stop_propagation)
            modifiers.push('stopPropagation');
        dispatch_dev('SvelteDOMAddEventListener', { node, event, handler, modifiers });
        const dispose = listen(node, event, handler, options);
        return () => {
            dispatch_dev('SvelteDOMRemoveEventListener', { node, event, handler, modifiers });
            dispose();
        };
    }
    function attr_dev(node, attribute, value) {
        attr(node, attribute, value);
        if (value == null)
            dispatch_dev('SvelteDOMRemoveAttribute', { node, attribute });
        else
            dispatch_dev('SvelteDOMSetAttribute', { node, attribute, value });
    }
    function set_data_dev(text, data) {
        data = '' + data;
        if (text.wholeText === data)
            return;
        dispatch_dev('SvelteDOMSetData', { node: text, data });
        text.data = data;
    }
    function validate_slots(name, slot, keys) {
        for (const slot_key of Object.keys(slot)) {
            if (!~keys.indexOf(slot_key)) {
                console.warn(`<${name}> received an unexpected slot "${slot_key}".`);
            }
        }
    }
    /**
     * Base class for Svelte components with some minor dev-enhancements. Used when dev=true.
     */
    class SvelteComponentDev extends SvelteComponent {
        constructor(options) {
            if (!options || (!options.target && !options.$$inline)) {
                throw new Error("'target' is a required option");
            }
            super();
        }
        $destroy() {
            super.$destroy();
            this.$destroy = () => {
                console.warn('Component was already destroyed'); // eslint-disable-line no-console
            };
        }
        $capture_state() { }
        $inject_state() { }
    }

    /* ..\controller\website\sign-up\src\App.svelte generated by Svelte v3.31.0 */

    const { Object: Object_1 } = globals;
    const file = "..\\controller\\website\\sign-up\\src\\App.svelte";

    function create_fragment(ctx) {
    	let main;
    	let div1;
    	let table;
    	let tr0;
    	let td0;
    	let t1;
    	let tr1;
    	let td1;
    	let t3;
    	let td2;
    	let input0;
    	let t4;
    	let tr2;
    	let td3;
    	let t6;
    	let td4;
    	let input1;
    	let t7;
    	let tr3;
    	let td5;
    	let t9;
    	let td6;
    	let input2;
    	let t10;
    	let tr4;
    	let td7;
    	let div0;
    	let t11_value = /*signUp*/ ctx[0].validation.error + "";
    	let t11;
    	let t12;
    	let button;
    	let mounted;
    	let dispose;

    	const block = {
    		c: function create() {
    			main = element("main");
    			div1 = element("div");
    			table = element("table");
    			tr0 = element("tr");
    			td0 = element("td");
    			td0.textContent = "會員註冊1";
    			t1 = space();
    			tr1 = element("tr");
    			td1 = element("td");
    			td1.textContent = "帳號：";
    			t3 = space();
    			td2 = element("td");
    			input0 = element("input");
    			t4 = space();
    			tr2 = element("tr");
    			td3 = element("td");
    			td3.textContent = "密碼：";
    			t6 = space();
    			td4 = element("td");
    			input1 = element("input");
    			t7 = space();
    			tr3 = element("tr");
    			td5 = element("td");
    			td5.textContent = "確認密碼：";
    			t9 = space();
    			td6 = element("td");
    			input2 = element("input");
    			t10 = space();
    			tr4 = element("tr");
    			td7 = element("td");
    			div0 = element("div");
    			t11 = text(t11_value);
    			t12 = space();
    			button = element("button");
    			button.textContent = "註冊";
    			attr_dev(td0, "class", "text-light bg-dark text-center text-title svelte-pw4mc8");
    			attr_dev(td0, "colspan", "2");
    			add_location(td0, file, 89, 16, 2870);
    			add_location(tr0, file, 89, 12, 2866);
    			attr_dev(td1, "class", "text-right w-25");
    			add_location(td1, file, 91, 16, 2985);
    			attr_dev(input0, "type", "email");
    			attr_dev(input0, "placeholder", "請輸入Email");
    			add_location(input0, file, 92, 33, 3055);
    			attr_dev(td2, "class", "w-75");
    			add_location(td2, file, 92, 16, 3038);
    			add_location(tr1, file, 90, 12, 2964);
    			attr_dev(td3, "class", "text-right w-25");
    			add_location(td3, file, 95, 16, 3183);
    			attr_dev(input1, "type", "password");
    			attr_dev(input1, "placeholder", "請輸入密碼");
    			add_location(input1, file, 96, 33, 3253);
    			attr_dev(td4, "class", "w-75");
    			add_location(td4, file, 96, 16, 3236);
    			add_location(tr2, file, 94, 12, 3162);
    			attr_dev(td5, "class", "text-right w-25");
    			add_location(td5, file, 99, 16, 3382);
    			attr_dev(input2, "type", "password");
    			attr_dev(input2, "placeholder", "再確認一次密碼");
    			add_location(input2, file, 100, 33, 3454);
    			attr_dev(td6, "class", "w-75");
    			add_location(td6, file, 100, 16, 3437);
    			add_location(tr3, file, 98, 12, 3361);
    			attr_dev(div0, "class", "text-danger p-3");
    			add_location(div0, file, 104, 20, 3655);
    			attr_dev(button, "type", "button");
    			attr_dev(button, "class", "btn btn-success");
    			add_location(button, file, 105, 20, 3736);
    			attr_dev(td7, "colspan", "2");
    			attr_dev(td7, "class", "text-center");
    			add_location(td7, file, 103, 16, 3598);
    			add_location(tr4, file, 102, 12, 3577);
    			attr_dev(table, "class", "table");
    			add_location(table, file, 88, 8, 2832);
    			attr_dev(div1, "class", "p-4");
    			add_location(div1, file, 87, 4, 2806);
    			attr_dev(main, "class", "svelte-pw4mc8");
    			add_location(main, file, 86, 0, 2795);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, main, anchor);
    			append_dev(main, div1);
    			append_dev(div1, table);
    			append_dev(table, tr0);
    			append_dev(tr0, td0);
    			append_dev(table, t1);
    			append_dev(table, tr1);
    			append_dev(tr1, td1);
    			append_dev(tr1, t3);
    			append_dev(tr1, td2);
    			append_dev(td2, input0);
    			set_input_value(input0, /*signUp*/ ctx[0].account);
    			append_dev(table, t4);
    			append_dev(table, tr2);
    			append_dev(tr2, td3);
    			append_dev(tr2, t6);
    			append_dev(tr2, td4);
    			append_dev(td4, input1);
    			set_input_value(input1, /*signUp*/ ctx[0].password);
    			append_dev(table, t7);
    			append_dev(table, tr3);
    			append_dev(tr3, td5);
    			append_dev(tr3, t9);
    			append_dev(tr3, td6);
    			append_dev(td6, input2);
    			set_input_value(input2, /*signUp*/ ctx[0].password_confirmation);
    			append_dev(table, t10);
    			append_dev(table, tr4);
    			append_dev(tr4, td7);
    			append_dev(td7, div0);
    			append_dev(div0, t11);
    			append_dev(td7, t12);
    			append_dev(td7, button);

    			if (!mounted) {
    				dispose = [
    					listen_dev(input0, "input", /*input0_input_handler*/ ctx[2]),
    					listen_dev(input1, "input", /*input1_input_handler*/ ctx[3]),
    					listen_dev(input2, "input", /*input2_input_handler*/ ctx[4]),
    					listen_dev(button, "click", /*click_handler*/ ctx[5], false, false, false)
    				];

    				mounted = true;
    			}
    		},
    		p: function update(ctx, [dirty]) {
    			if (dirty & /*signUp*/ 1 && input0.value !== /*signUp*/ ctx[0].account) {
    				set_input_value(input0, /*signUp*/ ctx[0].account);
    			}

    			if (dirty & /*signUp*/ 1 && input1.value !== /*signUp*/ ctx[0].password) {
    				set_input_value(input1, /*signUp*/ ctx[0].password);
    			}

    			if (dirty & /*signUp*/ 1 && input2.value !== /*signUp*/ ctx[0].password_confirmation) {
    				set_input_value(input2, /*signUp*/ ctx[0].password_confirmation);
    			}

    			if (dirty & /*signUp*/ 1 && t11_value !== (t11_value = /*signUp*/ ctx[0].validation.error + "")) set_data_dev(t11, t11_value);
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(main);
    			mounted = false;
    			run_all(dispose);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance($$self, $$props, $$invalidate) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots("App", slots, []);

    	let { signUp = {
    		account: "",
    		password: "",
    		password_confirmation: "",
    		encrypt: "",
    		encrypt_confirmation: "",
    		validation: { error: "" }
    	} } = $$props;

    	let child;
    	let is_sign_up_process = false;

    	function handleSignUp() {
    		if (is_sign_up_process) {
    			return;
    		}

    		$$invalidate(0, signUp.validation.error = "", signUp);

    		if (signUp.account.trim() == "") {
    			$$invalidate(0, signUp.validation.error = "Required email address", signUp);
    			return;
    		} else {
    			if (!signUp.account.match(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
    				$$invalidate(0, signUp.validation.error = "Please enter a valid email true", signUp);
    				return;
    			}
    		}

    		if (signUp.password.trim() == "") {
    			$$invalidate(0, signUp.validation.error = "Required password", signUp);
    			return;
    		}

    		if (signUp.password_confirmation.trim() == "") {
    			$$invalidate(0, signUp.validation.error = "Required password_confirmation", signUp);
    			return;
    		}

    		is_sign_up_process = true;

    		axios.post("/user/auth/sign-up", {
    			email: signUp.account,
    			password: signUp.password,
    			password_confirmation: signUp.password_confirmation
    		}).then(response => {
    			is_sign_up_process = false;

    			if (typeof response.data != "undefined") {
    				if (response.data.Status == "success") {
    					location.href = "/index";
    				} else {
    					let msg = null;

    					if (typeof response.data.error != "undefined") {
    						let error_data = response.data.error;

    						if (error_data.code > 0) {
    							msg = error_data.message;
    						} else {
    							msg = error_data.message[Object.keys(error_data.message)[0]][0];
    						}
    					} else {
    						if (typeof response.data.Message != "undefined") {
    							msg = response.data.Message;
    						} else {
    							msg = "系統出現異常，請稍後再試！";
    						}
    					}

    					$$invalidate(0, signUp.validation.error = msg, signUp);
    				}
    			}
    		}).catch(function (error) {
    			is_sign_up_process = false;
    			child.handleCreateNotification("請稍候再試，謝謝", "danger", 3000);
    		});
    	}

    	const writable_props = ["signUp"];

    	Object_1.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<App> was created with unknown prop '${key}'`);
    	});

    	function input0_input_handler() {
    		signUp.account = this.value;
    		$$invalidate(0, signUp);
    	}

    	function input1_input_handler() {
    		signUp.password = this.value;
    		$$invalidate(0, signUp);
    	}

    	function input2_input_handler() {
    		signUp.password_confirmation = this.value;
    		$$invalidate(0, signUp);
    	}

    	const click_handler = () => handleSignUp();

    	$$self.$$set = $$props => {
    		if ("signUp" in $$props) $$invalidate(0, signUp = $$props.signUp);
    	};

    	$$self.$capture_state = () => ({
    		signUp,
    		child,
    		is_sign_up_process,
    		handleSignUp
    	});

    	$$self.$inject_state = $$props => {
    		if ("signUp" in $$props) $$invalidate(0, signUp = $$props.signUp);
    		if ("child" in $$props) child = $$props.child;
    		if ("is_sign_up_process" in $$props) is_sign_up_process = $$props.is_sign_up_process;
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [
    		signUp,
    		handleSignUp,
    		input0_input_handler,
    		input1_input_handler,
    		input2_input_handler,
    		click_handler
    	];
    }

    class App extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance, create_fragment, safe_not_equal, { signUp: 0 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "App",
    			options,
    			id: create_fragment.name
    		});
    	}

    	get signUp() {
    		throw new Error("<App>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set signUp(value) {
    		throw new Error("<App>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    const app = new App({
    	target: document.getElementById('signUpContainer'),
    	props: {
    		name: 'Sign Up'
    	}
    });

    return app;

}());
//# sourceMappingURL=bundle.js.map
