package user_router

import (
	"github.com/gin-gonic/gin"
	"shop/app/core-module/auth"
	"shop/app/http/middleware"
	"shop/app/http/controllers/website/user/auth"
)

func Router(route *gin.Engine){
	user := route.Group("/user", middleware.EnableCookieSession())
	{
		user_auth_sign := user.Group("/", auth.CheckAuthSession())
		{
			user_auth_sign.GET("/auth/sign-in", user_auth.UserAuthSignInPage)
			user_auth_sign.GET("/auth/sign-up", user_auth.UserAuthSignUpPage)
		}

		user.POST("/auth/sign-in", user_auth.UserAuthSignInProcess)
		user.POST("/auth/sign-up", user_auth.UserAuthSignUpProcess)
		user.GET("/auth/sign-out", user_auth.UserAuthSignOutProcess)
	}
}