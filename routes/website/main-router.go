package main_router

import (
	"github.com/gin-gonic/gin"
	"shop/app/http/middleware"
	"shop/app/http/controllers/website/main"
)

func Router(route *gin.Engine) {
	main := route.Group("/", middleware.EnableCookieSession())
	{
		main.GET("/index", main_site.MainPage)
	}
}